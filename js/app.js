// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter', ['ionic', 'starter.controllers', 'ngCordova','ngAnimate']);
//angular.module('starter', ['ionic'])

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

app.config(function($ionicConfigProvider) {
  $ionicConfigProvider.views.maxCache(1);
});

app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

  .state('app.intro', {
    url: "/intro",
    views: {
      'menuContent': {
        templateUrl: "templates/intro.html",
        controller: 'IntroCtrl'
      }
    }
  })

  .state('app.character', {
    url: "/character",
    views: {
      'menuContent': {
        templateUrl: "templates/character.html",
        controller: 'CharacterCtrl'
      }
    }
  })
  .state('app.introsur', {
    url: "/introsur",
    views: {
      'menuContent': {
        templateUrl: "templates/introsur.html"
      }
    }
  })
  .state('app.suvybef', {
    url: "/suvybef",
    views: {
      'menuContent': {
        templateUrl: "templates/suvybef.html",
        controller: 'SuvybefCtrl'
      }
    }
  })

  .state('app.congratulations', {
    url: "/survay/",
    views: {
      'menuContent': {
        templateUrl: "templates/congratulations.html",
        controller: 'CongratulationsCtrl'
      }
    }
  })
  .state('app.survay', {
    url: "/survay/:survayId",
    views: {
      'menuContent': {
        templateUrl: "templates/survay.html",
        controller: 'SurvayCtrl'
      }
    }
  })
  .state('app.review', {
    url: "/review/:reviewId",
    views: {
      'menuContent': {
        templateUrl: "templates/review.html",
        controller: 'reviewCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/intro');
});

app.factory('UserService', function() {

  var items = [
  {"id":1,"question":"What drug are you thinking about using? "},
  {"id":2,"question":"Where will you be when you use?"},
  {"id":3,"question":"Who will you be with?"},
  {"id":4,"question":"Do you feel like you will be comfortable and safe in this situation? Why or why not?"},
  {"id":5,"question":"What do you hope to get out of this experience? WHY are you using (or not using)?"},
  {"id":6,"question":"Have you used this drug before? What was your experience?"},
  {"id":7,"question":"What do you know about this drug? Where did you get your information?"},
  {"id":8,"question":"Where will you get the drug? Do you trust your source?"},
  {"id":9,"question":"Are you on any other medications, drugs or alcohol?  How will they react?"},
  {"id":10,"question":"How are you feeling right now? How will using affect your mood/health right away? Later?"},
  {"id":11,"question":"How will you get home? Will your transportation be safe if you choose to use?"},
  {"id":12,"question":"If something goes wrong, where will you go for help or support?"}
  ];
  var character = [
  {"id":1,"character":"character-chicken","congratulations":"endingscreen-chicken"},
  {"id":2,"character":"character-raccoon","congratulations":"endingscreen-raccoon"},
  {"id":3,"character":"character-squirrel","congratulations":"endingscreen-squirrel"}
  ];
  return {
      question : items,
      character : character,
  };
});

app.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}]);


