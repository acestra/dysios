angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal) {
 /*
  screen_width  =  window.screen.width;
  screen_height  =  window.screen.height;

  alert(screen_width);
  alert(screen_height);
*/
})
.controller('IntroCtrl', function($scope, $ionicModal, $timeout,$ionicPopover, UserService, $localstorage, $cordovaInAppBrowser) {

  var options = {
    location: 'yes',
    clearcache: 'yes',
    toolbar: 'yes'
  };

$scope.openLinkDYS = function() {
      $cordovaInAppBrowser.open('https://skylarkyouth.org', '_system', options)
      .then(function(event) {
        // success
      })
      .catch(function(event) {
        // error
      });
}
$scope.openLink = function() {
      $cordovaInAppBrowser.open('http://www.mindyourmind.ca', '_system', options)
      .then(function(event) {
        // success
      })
      .catch(function(event) {
        // error
      });
}

  $scope.items = UserService.question;

  angular.forEach($scope.items, function(item) {

    var objval = 'ques_'+item.id;

    $localstorage.setObject(objval , { });

  });

  $ionicPopover.fromTemplateUrl('templates/credits.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
  $scope.openPopoverInstructions = function($event) {
    $scope.popover.show($event);
  };
    $scope.closePopover = function() {
    $scope.popover.hide();
  };
    $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  $scope.$on('popover.hidden', function() {
    // Execute action
  });
  
  $scope.$on('popover.removed', function() {
    // Execute action
  });
})
.controller('SuvybefCtrl', function($scope, $ionicModal,$ionicPopover) {

  $ionicPopover.fromTemplateUrl('templates/instructions.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });


  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  // Execute action on hide popover
  $scope.$on('popover.hidden', function() {
    // Execute action
  });
  // Execute action on remove popover
  $scope.$on('popover.removed', function() {
    // Execute action
  });
})

.controller('SlideController', function($scope) {
  
  $scope.myActiveSlide = 1;
  
})


.controller('SurvayCtrl', function($scope, $stateParams, $ionicPopup, $state, $cordovaInAppBrowser, $rootScope, $localstorage, $timeout, UserService, $cordovaSplashscreen) {

/*$cordovaSplashscreen.show();

  $timeout(function() {
    $cordovaSplashscreen.hide();
  }, 30);
*/
  $scope.class = "";
  $scope.question = "";
  $scope.background = "";
  $scope.character = "";
  $scope.character_owl = "";
  $scope.character_div = "";
  $scope.callout = "";


  if ($stateParams.survayId==1) {
    $scope.class = "main-survay survay-full-tree";
    $scope.background = "background-survay-initial background-"+$stateParams.survayId;
    $scope.character_owl = "owl col-offset-58";
    $scope.character = UserService.character[$localstorage.get('Character')-1].character+" character-img col-offset-10";
    $scope.character_div = "character_div";
    $scope.question = "survay-center-first";
    $scope.callout = "left callout-"+$stateParams.survayId;
  }
  else if ($stateParams.survayId==UserService.question.length) {
    $scope.class = "main-last survay-full-treeq";
    $scope.background = "background-survay background-"+$stateParams.survayId;
    $scope.character_owl = "owl-last col-offset-58";
    $scope.character = UserService.character[$localstorage.get('Character')-1].character+" character-img-last col-offset-10";
    $scope.character_div = "character-div-last";
    $scope.question = "survay-center-last";
    $scope.callout = "left callout-"+$stateParams.survayId;
  }
  else
  {
    var myEl = angular.element( document.querySelector( 'div.Character' ) );
    
    if(myEl.hasClass('character-img')) {
      myEl.removeClass('character-img'); 
    }
    
    //jQuery( "div.Character " ).removeClass( "character-img" )
    $scope.background = "background-survay background-"+$stateParams.survayId;
    if ($stateParams.survayId%2 == 0)
    {
      $scope.class = "main-even survay-full-treeq survay-full-tree main-survay-"+$stateParams.survayId;
      $scope.character_owl = "owl-even col-offset-10";
      $scope.character = UserService.character[$localstorage.get('Character')-1].character+" character-img-even";
      $scope.character_div = "character-div-even";
      $scope.question = "survay-center-even";
      $scope.callout = "right callout-"+$stateParams.survayId;
    }
    else
    {
      $scope.class = "main-odd survay-full-treeq survay-full-tree main-survay-"+$stateParams.survayId;
      $scope.character_owl = "owl-odd col-offset-58";
      $scope.character = UserService.character[$localstorage.get('Character')-1].character+" character-img-odd col-offset-10";
      $scope.character_div = "character-div-odd";
      $scope.question = "survay-center-odd";
      $scope.callout = "left callout-"+$stateParams.survayId;
    }
  }
  
//  document.getElementById("survay-full-tree").style["padding-top"] = "0";

  var options = {
    location: 'yes',
    clearcache: 'yes',
    toolbar: 'yes'
  };

  $rootScope.$on('$cordovaInAppBrowser:loadstart', function(e, event){

  });

  $rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event){
    // insert CSS via code / file
    $cordovaInAppBrowser.insertCSS({
      code: 'body {background-color:blue;}'
    });

    // insert Javascript via code / file
    $cordovaInAppBrowser.executeScript({
      file: 'script.js'
    });
  });

  $rootScope.$on('$cordovaInAppBrowser:loaderror', function(e, event){

  });

  $rootScope.$on('$cordovaInAppBrowser:exit', function(e, event){
    ionic.Platform.exitApp();
    navigator.app.exitApp() 
  });

  $scope.items = UserService.question;

  $scope.currentIndex = 0;

  $scope.next = function () {
        $scope.currentIndex++;
        if($scope.currentIndex>$scope.items.length)
        { $scope.currentIndex = 0;}
  };

  var count  = parseInt($stateParams.survayId)+1;
  if($scope.items.length>=count)
    $scope.itemnext = count;

  $scope.item = $scope.items[$stateParams.survayId-1];

  $scope.formData = {};
  $scope.formData.answer = "";

  $scope.isThisDisabled = function(){
    if($scope.formData.answer=="")
    {
      return true;
    }
    return false;
  }

  $scope.doneEdit = function() {
    var objval = 'ques_'+$scope.items[$stateParams.survayId-1]['id'];

    $localstorage.setObject(objval , {
      id : $scope.items[$stateParams.survayId-1]['id'],
      answer: $scope.formData.answer
    });

    var post = $localstorage.getObject(objval);
  };

  // A confirm dialog
  $scope.showConfirm = function() {
    var confirmPopup = $ionicPopup.confirm({
      title: '<img src="./img/text/exit-module.png" class="col col-33 col-offset-33">',
      template: '<div class="text-center">We’re sorry to see you go. Let us know why and you could win a prize </div>',
      cancelText: '<img src="./img/buttons/button-module1-no.png" class="col">', 
      cancelType: 'button-clear',
      okText: '<img src="./img/buttons/button-module1-yes.png" class="col">', 
      okType: 'button-clear', 
    });
    confirmPopup.then(function(res) {
      if(res) {
        $cordovaInAppBrowser.open('https://skylarkyouth.typeform.com/to/TlWMlK', '_system', options)
        .then(function(event) {
          // success
        })
        .catch(function(event) {
          // error
        });
      } else {
        $state.go('app.intro');
      }
    });
  };

  var options = {
    "direction"        : "down", // 'left|right|up|down', default 'left' (which is like 'next')
    "duration"         :  800, // in milliseconds (ms), default 400
    "slowdownfactor"   :    1, // overlap views (higher number is more) or no overlap (1), default 4
    "iosdelay"         :  100, // ms to wait for the iOS webview to update before animation kicks in, default 60
    "androiddelay"     :  150, // same as above but for Android, default 70
    "winphonedelay"    :  250, // same as above but for Windows Phone, default 200,
/*    "fixedPixelsTop"   :    0, // the number of pixels of your fixed header, default 0 (iOS only)
    "fixedPixelsBottom":   60  // the number of pixels of your fixed footer (f.i. a tab bar), default 0 (iOS only)*/
  };
  //window.plugins.nativepagetransitions.slide(options);

})


.controller('CharacterCtrl', function($scope, $ionicSlideBoxDelegate, $localstorage) {

  $scope.next = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.previous = function() {
    $ionicSlideBoxDelegate.previous();
  };
  if($localstorage.get('Character'))
  {
    $scope.slideIndex = $localstorage.get('Character')-1;
    $scope.ActiveSlide = $localstorage.get('Character')-1;
    $localstorage.set('Character',$scope.slideIndex = $localstorage.get('Character'));
  }
  else
  {
    $scope.slideIndex = 0;
    $scope.ActiveSlide = 0;
    $localstorage.set('Character',$scope.slideIndex=1);
  }

  $scope.slideChanged = function(index) {
    $scope.slideIndex = index+1;
    $localstorage.set('Character',$scope.slideIndex);
  };
})

.controller('CongratulationsCtrl', function($scope, $ionicPopover, $state, $cordovaInAppBrowser, $rootScope, $localstorage, $ionicSlideBoxDelegate, $cordovaSocialSharing, UserService) {

  $scope.items = UserService.question;

  $scope.products = [];

  var html = "<html><body>"

  angular.forEach($scope.items, function(item) {

    var objval = 'ques_'+item.id;

    var post = $localstorage.getObject(objval);
    if(post['answer'])
    {
    	html += "<div> <b>Question:</b>"+$scope.items[item.id-1]['question']+"<br><b>Answer:</b>"+post['answer']+"<br><br></div>";
        $scope.products.push({id: item.id, question: $scope.items[item.id-1]['question'], answer: post['answer']});
    }
    else
    {
    	html += "<div> <b>Question:</b>"+$scope.items[item.id-1]['question']+"<br><b>Answer:</b>Not answered<br><br></div>";
        $scope.products.push({id: item.id, question: $scope.items[item.id-1]['question'], answer: 'Not Answered'});
    }

  });

  html += "</body></html>";


$scope.congratulations = UserService.character[$localstorage.get('Character')-1].congratulations;

  $scope.shareViaEmail = function() {
    var message  = html;
    var subject = "Share";

    window.plugins.socialsharing.available(function(isAvailable) {
	// the boolean is only false on iOS < 6
	if (isAvailable) {
		window.plugins.socialsharing.share( message, subject , null , null  );
	}
	else
	{
		alert("Manickam");
	}
   });
  }
   var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'yes'
    };
$scope.openLinkComp = function() {
      $cordovaInAppBrowser.open('https://skylarkyouth.typeform.com/to/TlWMlK', '_system', options)
      .then(function(event) {
        // success
      })
      .catch(function(event) {
        // error
      });
}

$scope.openLinkDYS = function() {
      $cordovaInAppBrowser.open('https://skylarkyouth.org', '_system', options)
      .then(function(event) {
        // success
      })
      .catch(function(event) {
        // error
      });
}
$scope.openLink = function() {
      $cordovaInAppBrowser.open('http://www.mindyourmind.ca', '_system', options)
      .then(function(event) {
        // success
      })
      .catch(function(event) {
        // error
      });
}
  $scope.next = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.previous = function() {
    $ionicSlideBoxDelegate.previous();
  };

$scope.slideIndex = 1;
  // Called each time the slide changes
  $scope.slideChanged = function(index) {
    $scope.slideIndex = index+1;
  };


  $rootScope.$on('$cordovaInAppBrowser:loadstart', function(e, event){

  });

  $rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event){
    // insert CSS via code / file
    $cordovaInAppBrowser.insertCSS({
      code: 'body {background-color:blue;}'
    });

    // insert Javascript via code / file
    $cordovaInAppBrowser.executeScript({
      file: 'script.js'
    });
  });

  $rootScope.$on('$cordovaInAppBrowser:loaderror', function(e, event){

  });

  $rootScope.$on('$cordovaInAppBrowser:exit', function(e, event){
    ionic.Platform.exitApp();
    navigator.app.exitApp() 
  });
// .fromTemplateUrl() method
  $ionicPopover.fromTemplateUrl('templates/review.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  // Execute action on hide popover
  $scope.$on('popover.hidden', function() {
    // Execute action
  });
  // Execute action on remove popover
  $scope.$on('popover.removed', function() {
    // Execute action
  });

});
